function [stereoVid] = createStereoVideo(imgDirectory, nViews)
    imgs = loadImages(imgDirectory);
    stereoVid = repmat(struct('cdata', [], 'colormap', []), nViews, 1);
    T = createTransformations(imgs);
    T2 = imgToPanoramaCoordinates(T) ;
    sliceWidth = round(size(imgs,2)/nViews);
    panoSize = [size(imgs, 1) ceil(size(imgs, 2)+abs(T2{end}(1,3)))-sliceWidth*(nViews - 1)];
    fuckedUpFrames=zeros(nViews,1);
    for i=1:nViews
        imgSliceCenterX = repmat(round((i - 1)*sliceWidth + sliceWidth/2), size(imgs, 4), 1);
        [panoramaFrame, frameNotOK] = renderPanoramicFrame(panoSize, imgs, T2, imgSliceCenterX, sliceWidth/2);
        if ~frameNotOK
            panoramaFrame(isnan(panoramaFrame)) = 0;
            panoramaFrame=im2uint8(panoramaFrame);
            stereoVid(i) = im2frame(panoramaFrame);
        else
            i
            fuckedUpFrames(i)=1;
        end
        
    end
stereoVid=stereoVid(find(fuckedUpFrames==0));
end