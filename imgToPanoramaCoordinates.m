function Tout=imgToPanoramaCoordinates 	(Tin)
  Tout = cell(length(Tin), 1);
  Tout{1} = eye(3);
    for i = 2:length(Tout)
       Tout{i} = Tout{i-1}*Tin{i - 1}; 
    end
        
end
