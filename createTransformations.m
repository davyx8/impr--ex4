function Tin = createTransformations(imgs)
    [~,~,~,picNum]=size(imgs)
    Tin=cell(ceil(picNum),1);   
    size(Tin)
    for i=1:picNum-1
    id=eye(3);
    [dx dy] = findTransformations(imgs(:,:,:,i),imgs(:,:,:,i+1),3,3);
    id(1,3)=dx;
    id(2,3)=dy;
    Tin{ceil(i)}=id;
    end
    Tin{picNum}=Tin{picNum-1}+1;
end

