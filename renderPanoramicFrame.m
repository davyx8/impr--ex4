<<<<<<< HEAD
function [panoramaFrame, frameNotOK] = renderPanoramicFrame(panoSize, imgs, T, imgSliceCenterX, halfSliceWidthX)
frameNotOK=true;
panoramaFrame = zeros(panoSize(1),panoSize(2),3);
TransformedCentersX = zeros(size(imgSliceCenterX));
bounds = zeros(length(imgSliceCenterX)+ 1,1);
offset=zeros(size(T));
for i=1:length(T)
    offset(i)=T{i}(1,3);
end
for i=1:length(imgSliceCenterX)-1
    TransformedCentersX(i) = imgSliceCenterX(i)-offset(i);
end

bounds(1) = TransformedCentersX(1) - halfSliceWidthX;
for i=1:length(imgSliceCenterX)-1
    bounds(i+1) = (TransformedCentersX(i) + TransformedCentersX(i+1))/2;
end
bounds(length(imgSliceCenterX)+1) = TransformedCentersX(length(imgSliceCenterX)) + halfSliceWidthX;
bounds=round(bounds);
Height = panoSize(1);
xIdx=1;
%bounds
for i = 1:numel(T)-2
    img=imgs(:,:,:,i);
    L = bounds(i) + 1;
    R = bounds(i+1);
    Width = R -L + 1;
    [X, Y] = meshgrid( round(L):round(R), 1:Height);
    stripCo = [X(:), Y(:), ones(numel(X),1)];
    stripCo=T{i}*stripCo';
    X = reshape(stripCo(1,:), (Height),(Width));
    Y = reshape(stripCo(2,:), (Height),(Width));
    im2r = interp2(img(:,:,1),X,Y,'linear');
    im2g = interp2(img(:,:,2),X,Y,'linear');
    im2b = interp2(img(:,:,3),X,Y,'linear');
    finalStrip = cat(3, im2r, im2g, im2b);
    size(finalStrip);
    panoramaFrame(:, xIdx:xIdx + size(finalStrip, 2)-1, :) = finalStrip;
    xIdx=xIdx + size(finalStrip, 2);
end
for i=1:size(panoramaFrame,2)
    if( sum(~isnan(panoramaFrame(:,i,:)))==0 )
        frameNotOK = true
        return
    end
end
frameNotOK = false;
=======
function [panoramaFrame, frameNotOK] = renderPanoramicFrame(panoSize, imgs, T, imgSliceCenterX, halfSliceWidthX)    
display('size:')
size(imgs, 4)
numel(T)
frameNotOK=true;
panoramaFrame = zeros(panoSize(1),panoSize(2),3);
    TransformedCentersX = zeros(size(imgSliceCenterX));
    bounds = zeros(length(imgSliceCenterX)+ 1,1);
    offset=zeros(size(T));
    for i=1:length(T)
     offset(i)=T{i}(1,3);
    end
    for i=1:length(imgSliceCenterX)-1
         TransformedCentersX(i) = imgSliceCenterX(i)-offset(i);
           %   T{i}*  imgSliceCenterX(i)
          %TransformedCentersX(i)=T{i}*imgSliceCenterX(i)
    end

    bounds(1) = TransformedCentersX(1) - halfSliceWidthX;
    for i=1:length(imgSliceCenterX)-1
        if (i==70 || i==69)
            'abandon all hope'
            TransformedCentersX(i)
            TransformedCentersX(i+1)
        end
        bounds(i+1) = (TransformedCentersX(i) + TransformedCentersX(i+1))/2;
    end
    bounds(length(imgSliceCenterX)+1) = TransformedCentersX(length(imgSliceCenterX)) + halfSliceWidthX;
    bounds=round(bounds);
    Height = panoSize(1);     
    xIdx=1;
    %bounds
     for i = 1:numel(T)-2
        img=imgs(:,:,:,i);
        %   Width = bounds(i+1) - bounds(i);      
         i
     
         %TransformedCentersX(i)

         %TransformedCentersX(i+1)
         L = bounds(i) + 1
          R = bounds(i+1)
%         
         Width = R -L + 1;       
        [X, Y] = meshgrid( round(L):round(R), 1:Height);
         stripCo = [X(:), Y(:), ones(numel(X),1)];
%         size(stripCo)
%         size(T{i})
%         i
         stripCo=T{i}*stripCo';
%         round(Height)
%         round(Width)
%         numel(stripCo(1,:))
%         Height

% 
% 
% sCor = [...
%             reshape(X, 1, Width * Height); ...
%             reshape(Y, 1, Width* Height); ...
%             ones(1, Width * Height)];        
%         
% 
%         sCor = inv(T{i}) * sCor;
%         
         X = reshape(stripCo(1,:), (Height),(Width));
     
         Y = reshape(stripCo(2,:), (Height),(Width)); 
         %size(Y)     
%         finalStrip = zeros(size(X));
% 
%         finalStrip(:,:,1) = interp2(imgs(:,:,1,i), X, Y, 'linear', 0);
%         finalStrip(:,:,2) = interp2(imgs(:,:,2,i), X, Y, 'linear', 0);
%         finalStrip(:,:,3) = interp2(imgs(:,:,3,i), X, Y, 'linear', 0);
%         finalStrip(isnan(finalStrip))=0;
%         panoramaFrame(1:panoSize(1), L:R, :) = finalStrip;
%         
%         
        
        
        im2r = interp2(img(:,:,1),X,Y,'linear');
        im2g = interp2(img(:,:,2),X,Y,'linear');
        im2b = interp2(img(:,:,3),X,Y,'linear');
        finalStrip = cat(3, im2r, im2g, im2b);

  
        size(finalStrip);

        panoramaFrame(:, xIdx:xIdx + size(finalStrip, 2)-1, :) = finalStrip;
        xIdx=xIdx + size(finalStrip, 2);
     end

    frameNotOK = false;
>>>>>>> 1559cd9a301bf8e0b6384c8f1f6e6244ba25109b
end