function imgs = loadImages(directoryPath )
    directory  = dir(directoryPath);
    path = strcat(directoryPath, '/', directory(3).name);
    imSize = size(imread(path));
    imgs = zeros(imSize(1), imSize(2), imSize(3), numel(directory)-2);
    for i = 3:numel(directory)
        path = strcat(directoryPath, '/', directory(i).name)     ;
        imgs(:,:,:,i-2) =im2double(imread(path));
    end
    
end

